package controllers

import (
	"bitbucket.org/mlsdatatools/golang-angular-testing/models"
	// "fmt"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

func GetProducts(c echo.Context) error {
	models := models.FindAll()

	return c.JSON(http.StatusOK, models)
}

func GetProduct(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	model := models.Find(id)

	return c.JSON(http.StatusOK, model)
}

// func Create(c echo.Context) error {
// 	var product Product

// 	if err := c.Bind(&product); err != nil {
// 		return err
// 	}

// 	// db.Create(&product)

// 	model := models.Create(product)

// 	return c.JSON(http.StatusCreated, model)
// }
