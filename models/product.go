package models

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var db *gorm.DB
var err error

type Product struct {
	gorm.Model
	Code  string
	Price uint
}

func init() {
	db, err = gorm.Open("mysql", "root::/ssh@djos.36@/testdrive?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		panic("failed to connect database")
	}

	// defer db.Close()

	// Migrate the schema
	// db.AutoMigrate(&Product{})
}

func FindAll() []Product {
	var products []Product

	if err := db.Find(&products).Error; err != nil {
		fmt.Println(err)
	}

	return products
}

func Find(id int) Product {
	var product Product

	if err := db.First(&product, id).Error; err != nil {
		fmt.Println(err)
		// return echo.NewHTTPError(http.StatusNotFound, "Not found")
	}

	return product
}

// func Create(product Product) Product {
// 	// var product Product

// 	if err := c.Bind(&product); err != nil {
// 		return err
// 	}

// 	db.Create(&product)
// }
